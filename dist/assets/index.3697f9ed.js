(function(){const o=document.createElement("link").relList;if(o&&o.supports&&o.supports("modulepreload"))return;for(const e of document.querySelectorAll('link[rel="modulepreload"]'))s(e);new MutationObserver(e=>{for(const t of e)if(t.type==="childList")for(const i of t.addedNodes)i.tagName==="LINK"&&i.rel==="modulepreload"&&s(i)}).observe(document,{childList:!0,subtree:!0});function r(e){const t={};return e.integrity&&(t.integrity=e.integrity),e.referrerpolicy&&(t.referrerPolicy=e.referrerpolicy),e.crossorigin==="use-credentials"?t.credentials="include":e.crossorigin==="anonymous"?t.credentials="omit":t.credentials="same-origin",t}function s(e){if(e.ep)return;e.ep=!0;const t=r(e);fetch(e.href,t)}})();function c(n){let o=0;const r=s=>{o=s,n.innerHTML=`count is ${o}`};n.addEventListener("click",()=>r(o+1)),r(0)}document.querySelector("#app").innerHTML=`
  <div class="container mt-5">
    <div class="row">
      <div class="col-xs-12">
        <a href="https://pimentcss.webmonster.tech/" target="_blank">
          <img src="/logo-pimentcss.svg" class="logo" alt="Piment CSS logo" width="150" />
        </a>
        <h1 class="text-blue">Hello Piment CSS!</h1>
      </div>
      <div class="col-xs-12">
        <div class="card">
          <button class="btn-outlined-red" id="counter" type="button"></button>
        </div>
      </div>
      <div class="col-xs-12">
        <p class="read-the-docs">Click on the Piment CSS logo to learn cmore</p>
      </div>
    </div>
  </div>
  <footer class="text-center">
    <small class="text-center">Piment CSS v0.3.33 + Vite.js</small>
  </footer>
`;c(document.querySelector("#counter"));
