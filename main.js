import './pimentcss.css';
import { setupCounter } from './counter.js';

document.querySelector('#app').innerHTML = `
  <div class="container mt-5">
    <div class="row">
      <div class="col-xs-12">
        <a href="https://pimentcss.webmonster.tech/" target="_blank">
          <img src="/logo-pimentcss.svg" class="logo" alt="Piment CSS logo" width="150" />
        </a>
        <h1 class="text-blue">Hello Piment CSS!</h1>
      </div>
      <div class="col-xs-12">
        <div class="card">
          <button class="btn-outlined-red" id="counter" type="button"></button>
        </div>
      </div>
      <div class="col-xs-12">
        <p class="read-the-docs">Click on the Piment CSS logo to learn cmore</p>
      </div>
    </div>
  </div>
  <footer class="text-center">
    <small class="text-center">Piment CSS v0.3.33 + Vite.js</small>
  </footer>
`;

setupCounter(document.querySelector('#counter'));
